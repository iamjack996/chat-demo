const express = require('express');
const app = express();

const admin = require("firebase-admin");
const serviceAccount = require("../config/serviceAccountKey.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://chat-demo-5a060.firebaseio.com"
});

const DB = admin.database()
const Users = DB.ref('/User')

const ChatRoom = DB.ref('/ChatRoom')

const session = require('express-session')
const MemoryStore = session.MemoryStore;
app.use(session({
    secret: 'recommand128BytesRandomStringrecommand128BytesRandomStringrecommand', // 建议使用 128 个字符的随机字符串
    cookie: { maxAge: 60 * 1000 * 30 },
    resave: true,
    saveUninitialized: true,
    store: new MemoryStore()
}));


exports.index = function (req, res) {
    res.render('chat/index');
    // res.send('Greetings !!! from the ChatController! HELLO');
};

exports.home = function (req, res) {
    let { userName } = req.body
    let roomList = []

    ChatRoom
        .once('value', async snapshot => {
            snapshot.forEach(room => {
                roomList.push(room.key)
                // console.log(room.key)
            })
            console.log(roomList.length)
            res.render('chat/home', { userName, roomList })
        })
};

exports.createRoom = function (req, res) {
    let { roomKey, userName } = req.body
    let msgs = []

    let Room = DB.ref('/ChatRoom/' + roomKey)

    Room.child('msg')
        .once('value', async snapshot => {
            snapshot.forEach(roomMsg => {
                // roomMsg.time = (Date(roomMsg.created_time).getMonth() + 1) + "/" + Date(roomMsg.created_time).getDate()
                
                msgs.push(roomMsg.val())
            })
            // console.log(msgs)
            res.render('chat/index', { key: roomKey, user: userName, msgs })
        })
};


exports.auth = function (req, res) {
    res.render('login')
};

exports.login = function (req, res) {
    let { account } = req.body
    console.log(account)

    Users
        .orderByChild('account')
        .equalTo(account)
        .limitToFirst(1)
        .once('value', async snapshot => {
            let userInfo = {}
            snapshot.forEach(child => userInfo = child.val())

            if (!userInfo.account) {
                res.render('login');
            }
            userName = userInfo.account

            req.session.user = userName

            let roomList = []
            ChatRoom
                .once('value', async snapshot => {
                    snapshot.forEach(room => {
                        roomList.push(room.key)
                        // console.log(room.key)
                    })
                    res.render('chat/home', { roomList, userName: req.session.user })
                })
        })
};


exports.addUser = function (req, res) {
    Users.push({
        account: 'jack',
        password: '1234',
        created_time: Date.now(),
        updated_time: Date.now()
    })
};

exports.logout = function (req, res) {
    res.redirect('/chats/auth');
};