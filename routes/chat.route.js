const express = require('express');
const router = express.Router();
module.exports = router;

const chatController = require('../controllers/chat.controller');

router.get('/addUser', chatController.addUser);
router.get('/auth', chatController.auth);
router.post('/login', chatController.login);
router.get('/logout', chatController.logout);

router.use(function (req, res, next) {

  if (req.session.user) {
    next();
  } else {
    res.redirect('/chats/auth');
  }

})

router.get('/', chatController.index);
router.all('/home', chatController.home);
router.post('/createRoom', chatController.createRoom);