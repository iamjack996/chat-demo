const express = require('express');
const app = express();
const path = require('path');

// Load view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// For Form Request Use BodyParser
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/public', express.static('public')); // 依賴資源檔

// Session
const session = require('express-session');

app.use(session({
    secret: 'sessiontest',
    resave: true,
    saveUninitialized: true,
    cookie: { maxAge: 600 * 1000 }
}))

// Route
const chat = require('./routes/chat.route');
app.use('/chats', chat);

// DB
const admin = require("firebase-admin");
const DB = admin.database()

// Socket.io
const server = require('http').Server(app);
const io = require('socket.io')(server);

let onlineCount = 0   // 加入線上人數計數

const nsp = io.of('/chat');

nsp.on('connection', function (socket) {
    console.log('Someone connected');

    // socket.on('openRoom', function (key) {
    //     // socket.join(key)
    //     socket.join(key, function () {
    //         // console.log('socket.rooms');
    //         console.log(socket.rooms);
    //     });
    //     console.log('Key => ' + key)
    // });

    socket.on('join', function (data) {
        let { key, user } = data
        console.log('Join => ' + key)

        socket.join(key)

        let Room = DB.ref('/ChatRoom/' + key)

        Room
            .child('member')
            .orderByChild("user")
            .equalTo(user)
            .once('value', async snapshot => {
                let countThisUser = snapshot.numChildren();

                if (countThisUser == 0) {
                    Room.child('member').push({
                        user
                    })
                    Room
                        .child('member')
                        .once("value")
                        .then(async snapshot => {
                            let peopleCount = snapshot.numChildren();

                            if (peopleCount === 1) { //新開的房間
                                nsp.emit("newRoom", key);
                            }

                            console.log(peopleCount)
                            nsp.to(key).emit("roomOnline", peopleCount);

                            data.roomKey = key
                            data.name = user + '進入聊天室！'
                            data.msg = ''
                            nsp.to(key).emit("msg", data);
                        });
                }

            })


    })

    socket.on('leave', function (data) {
        let { key, user } = data
        console.log('Leave => ' + user)
        socket.leave(key)

        let Room = DB.ref('/ChatRoom/' + key)
        Room.child('member')
            .orderByChild("user")
            .equalTo(user)
            .once("value")
            .then(async snapshot => {
                // console.log(snapshot.val())
                snapshot.forEach(child => {
                    child.ref.remove()
                })

                Room.child('member').once("value")
                    .then(async snapshot => {
                        let peopleCount = snapshot.numChildren();

                        if (peopleCount == 0) {
                            Room.remove()
                            nsp.emit("removeRoom", key);
                        }

                        nsp.to(key).emit("roomOnline", peopleCount);

                        data.roomKey = key
                        data.name = user + '退出聊天室！'
                        data.msg = ''
                        nsp.to(key).emit("msg", data);
                    });
            });

    })

    // 有連線發生時增加人數
    onlineCount++;
    // 發送人數給網頁
    nsp.emit("online", onlineCount);

    socket.on("greet", () => {
        socket.emit("greet", onlineCount);
    });

    socket.on('disconnect', () => {
        // 有人離線了，扣人
        onlineCount = (onlineCount < 0) ? 0 : onlineCount -= 1;
        nsp.emit("online", onlineCount);
    });

    socket.on("send", (data) => {
        // 如果 msg 內容鍵值小於 2 等於是訊息傳送不完全
        // 因此我們直接 return ，終止函式執行。

        if (Object.keys(data).length < 3) return;

        // 廣播訊息到聊天室
        nsp.to(data.roomKey).emit("msg", data);
        // console.log(data.roomKey)

        data.time = Date.now()

        let { roomKey, name, msg, time } = data

        // console.log(data)

        let chat = DB.ref('/ChatRoom/' + roomKey)

        chat.child('msg').push({
            name,
            msg,
            created_time: time,
        })
    });
});

const homeIo = io.of('/home');

homeIo.on('connection', function (socket) {

})

server.listen(3000, () => { // 掛上3000 port
    console.log("Server Started. http://localhost:3000");
});